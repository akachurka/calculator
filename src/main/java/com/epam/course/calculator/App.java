package com.epam.course.calculator;

import java.util.Arrays;

/**
 * Simple console calculator that supports the following binary
 * operations:
 * 1) addition (subtraction)
 * 2) multiplication (division)
 * 3) involution
 */
public class App {

    public static void main(String[] args) {
        if (args.length != 3) {
            System.out.println("Two operands and a binary operation should be provided.");
            return;
        }
        try {
            Double left = Double.parseDouble(args[0]);
            Double right = Double.parseDouble(args[2]);
            String operation = args[1];

            Computer computer = new Computer();
            Double result = computer.calculate(Arrays.asList(left, right), operation);
            System.out.println(result);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
