package com.epam.course.calculator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BinaryOperator;

/**
 * The class instances acts as a computers capable
 * to perform calculations.
 */
public class Computer {

    // Container for supported operations.
    private static final Map<String, BinaryOperator<Double>> binaryOps =
            new HashMap<String, BinaryOperator<Double>>() {
        {
            put("+", (a, b) -> a + b);
            put("-", (a, b) -> a - b);
            put("*", (a, b) -> a * b);
            put("/", (a, b) -> a / b);
            put("^", Math::pow);
        }
    };

    private BinaryOperator<Double> defineOperator(String operation) {
        BinaryOperator<Double> operator = binaryOps.get(operation);
        if (operator == null) {
            throw new IllegalArgumentException("Unrecognized operation: " + operation);
        }
        return operator;
    }

    /**
     * Calculates a value using the given list of operands and an operation.
     * Only binary operations are supported for now.
     *
     * @param operands the given operands; keep in mind that order of operands is important
     *                 for non-commutative operations
     * @param operation a binary operation
     * @return calculation result
     */
    public Double calculate(List<Double> operands, String operation) {
        if (operands == null || operands.size() != 2) {
            throw new IllegalArgumentException("Operands list must contain exactly two " +
                    "values cause only binary operations are supported");
        }
        Double left = operands.get(0);
        Double right = operands.get(1);

        if (left == null || right == null) {
            throw new IllegalArgumentException("Operands must not be null");
        }
        if (operation == null || operation.isEmpty()) {
            throw new IllegalArgumentException("Operation must not be empty");
        }
        BinaryOperator<Double> operator = defineOperator(operation);
        return operator.apply(left, right);
    }
}