package com.epam.course;

import static org.junit.Assert.assertEquals;

import com.epam.course.calculator.Computer;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Arrays;
import java.util.List;

/**
 * A unit test for <code>Computer</code> instance.
 */
public class ComputerTest {

    private static Computer computer;
    private static List<Double> validOperands;

    @BeforeClass
    public static void init() {
        computer = new Computer();
        validOperands = Arrays.asList(3d, 2d);
    }

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testAddition() {
        testBinaryOperation(3d, "+", 5d, 8d);
    }

    @Test
    public void testSubtraction() {
        testBinaryOperation(6d, "-", 9d, -3d);
    }

    @Test
    public void testMultiplication() {
        testBinaryOperation(4d, "*", 3d, 12d);
    }

    @Test
    public void testDivision() {
        testBinaryOperation(5d, "/", 2d, 2.5);
    }

    @Test
    public void testInvolution() {
        testBinaryOperation(6d, "^", 2d, 36d);
    }

    private void testBinaryOperation(Double left, String operation, Double right, Double expectedResult) {
        Double actualResult = computer.calculate(Arrays.asList(left, right), operation);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testThatInvalidOperationIsHandledGracefully() {
        String invalidOperation = "%";
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Unrecognized operation: " + invalidOperation);
        computer.calculate(validOperands, invalidOperation);
    }

    @Test
    public void testThatEmptyOperationIsHandledGracefully() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Operation must not be empty");
        computer.calculate(validOperands, "");
    }

    /**
     * Will be deleted when operations other than binary ones are implemented.
     */
    @Test
    public void testThatIllegalNumberOfOperandsIsHandledGracefully() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Operands list must contain exactly two " +
                "values cause only binary operations are supported");
        computer.calculate(Arrays.asList(3d, 2d, 1d), "+");
    }
}